<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License
Ported to Drupal by Pavan Keshavamurthy - http://grahana.net/

Name       : Industrial
Description: A three-column, fixed-width blog design.
Version    : 1.0
Released   : 20071004

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
<!-- start header -->
<div id="logo">
<?php if ($logo): ?>
<a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" /> </a>
<?php endif; ?>
<h1 id='site-name'> <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?> </a> </h1>
</div>

<?php if ($primary_links): ?> <div id="menu"> <?php print theme('links', $primary_links); ?> </div> <?php endif; ?>
<?php if ($secondary_links): ?> <div id="menu"><?php print theme('links', $secondary_links); ?> </div> <?php endif; ?>
<!-- end header -->
<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
						<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
						<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
						<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
						<?php print $help; ?>
						<?php print $messages; ?>
						<?php print $content; ?>
	</div>
	<!-- end content -->
	<!-- start sidebars -->
	<?php if ($sidebar_right): ?>
	<div id="sidebar1" class="sidebar"><?php print($sidebar_right); ?></div>
	<?php endif; ?>
	<?php if ($sidebar_left): ?>
	<div id="sidebar2" class="sidebar"><?php print($sidebar_left); ?></div>
	<?php endif; ?>
	<!-- end sidebars -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<div id="footer">
    <p><?php print($footer_message); ?></p>
	<p>&copy;2007 All Rights Reserved. &nbsp;&bull;&nbsp; Design by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a>.</p>
</div>
</body>
</html>
